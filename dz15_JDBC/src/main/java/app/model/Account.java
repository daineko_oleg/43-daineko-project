package app.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;
import lombok.Builder;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class Account {

    private Long id;
    private String first_name;
    private String last_name;
    private String email;
    private Date created_at;
    private Date deleted_at;

}
