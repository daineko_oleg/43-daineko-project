package app.main;

import app.Accounts.WorkingWithAccounts;

import java.io.IOException;
import java.sql.*;

public class Main {
    public static void main(String[] args) throws SQLException, IOException {

        WorkingWithAccounts workingWithAccounts = new WorkingWithAccounts();

        workingWithAccounts.printInfo();
        workingWithAccounts.select();
    }
}
