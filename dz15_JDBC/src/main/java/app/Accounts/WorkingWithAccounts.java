package app.Accounts;

import app.model.Account;

import java.sql.*;
import java.util.Date;
import java.util.Scanner;

public class WorkingWithAccounts {

    private Scanner scanner = new Scanner(System.in);

    // соединение с базой данных
    private final Connection connection = DriverManager.getConnection(
            "jdbc:postgresql://localhost:5432/postgres",
            "postgres",
            "postgres"
    );

    public WorkingWithAccounts() throws SQLException {
    }

    public void viewingAccounts() throws SQLException {

        PreparedStatement preparedStatement = connection.prepareStatement("select * from account");

        // statement получил ответ от БД и положил в ResultSet
        // в обьекте лежит информация по нашему запросу
        ResultSet result = preparedStatement.executeQuery();

        while (result.next()) {
            Long id = result.getLong(1);
            String first_name = result.getString("first_name");
            String last_name = result.getString("last_name");
            String email = result.getString("email");
            Date created_at = result.getDate(5);
            Date deleted_at = result.getDate(6);

            Account account = Account.builder()
                    .id(id)
                    .first_name(first_name)
                    .last_name(last_name)
                    .email(email)
                    .created_at(created_at)
                    .deleted_at(deleted_at)
                    .build();
            System.out.println(account);
        }
    }

    // поиск по id
    public void searchAccountsByID(int accId) throws SQLException {

        PreparedStatement preparedStatement = connection.prepareStatement("select * from account WHERE id=?");
        preparedStatement.setInt(1, accId);

        ResultSet result = preparedStatement.executeQuery();

        while (result.next()) {
            Long id = result.getLong(1);
            String first_name = result.getString("first_name");
            String last_name = result.getString("last_name");
            String email = result.getString("email");
            Date created_at = result.getDate(5);
            Date deleted_at = result.getDate(6);

            Account account = Account.builder()
                    .id(id)
                    .first_name(first_name)
                    .last_name(last_name)
                    .email(email)
                    .created_at(created_at)
                    .deleted_at(deleted_at)
                    .build();
            System.out.println(account);
        }
    }

    // создание нового
    public void createNewAccount() throws SQLException {

        PreparedStatement preparedStatement = connection.prepareStatement
                ("INSERT INTO account (first_name, last_name, email, created_at) VALUES (?, ?, ?, ?)");
        long currentTime = System.currentTimeMillis();
        java.sql.Date date = new java.sql.Date(currentTime);

        scanner.nextLine();
        System.out.println("Введите имя");
        preparedStatement.setString(1, scanner.nextLine());
        System.out.println("Введите фамилию");
        preparedStatement.setString(2, scanner.nextLine());
        System.out.println("Введите почтовый адрес");
        preparedStatement.setString(3, scanner.nextLine());
        preparedStatement.setDate(4, date);

        preparedStatement.executeUpdate();
    }

    public void updateAccount(int upId) throws SQLException {

        PreparedStatement preparedStatement = connection.prepareStatement
                ("UPDATE account SET first_name=?, last_name=?, email=? WHERE id=?");

        preparedStatement.setInt(4, upId);
        scanner.nextLine();
        System.out.println("Введите имя");
        preparedStatement.setString(1, scanner.nextLine());
        System.out.println("Введите фамилию");
        preparedStatement.setString(2, scanner.nextLine());
        System.out.println("Введите почтовый адрес");
        preparedStatement.setString(3, scanner.nextLine());

        preparedStatement.executeUpdate();
    }

    public void deleteAccount(int delId) throws SQLException {

        PreparedStatement preparedStatement = connection.prepareStatement
                ("UPDATE account SET deleted_at=? WHERE id=?");

        preparedStatement.setInt(2, delId);
        long currentTime = System.currentTimeMillis();

        java.sql.Date date = new java.sql.Date(currentTime);
        preparedStatement.setDate(1, date);

        preparedStatement.executeUpdate();
    }

    public void printInfo() {
        System.out.println(
                "Что бы просмотреть все аккаунты, нажмите '1' \n" +
                        "Что бы найти аккаунт по ID, нажмите '2' \n" +
                        "Что бы добавить новый аккаунт, нажмите '3' \n" +
                        "Что бы редактировать аккаунт, нажмите '4' \n" +
                        "Чтобы удалить аккаунт, нажмите '5' \n");
    }

    public void select() throws SQLException {
        switch (scanner.nextInt()) {
            case 1:
                viewingAccounts();
                break;
            case 2:
                System.out.println("Введите ID: ");
                int accId = scanner.nextInt();
                searchAccountsByID(accId);
                break;
            case 3:
                createNewAccount();
                break;
            case 4:
                System.out.println("Введите ID: ");
                int upId = scanner.nextInt();
                updateAccount(upId);
                break;
            case 5:
                System.out.println("Введите ID: ");
                int delId = scanner.nextInt();
                deleteAccount(delId);
            default:
                System.out.println("Вы ввели неправильный номер");
                break;
        }
    }
}
