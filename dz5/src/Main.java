
import java.util.Random;

public class Main {

    public static void main(String[] args) {

        Human[] humans = new Human[(int) (Math.random() * 20)];

        String[] names = new String[]{"Владимир", "Петр", "Иван", "Семен", "Михаил", "Алексей", "Юрий"};
        String[] lastNames = new String[]{"Иванов", "Петров", "Сидоров", "Баширов", "Блабла", "Кваква", "Хрюхрю"};

        Random random = new Random();

        for (int i = 0; i < humans.length; i++) {
            int num1 = random.nextInt(names.length);
            //System.out.println(names[num1]);
            int num2 = random.nextInt(lastNames.length);
            //System.out.println(lastNames[num2]);
            int ages = (int) (Math.random() * 100);

            humans[i] = new Human(names[num1], lastNames[num2], ages);
        }

        for (int i = 0; i < humans.length; i++) {
            for (int j = 0; j < humans.length - i - 1; j++) {
                if (humans[j].getAge() > humans[j + 1].getAge()) {
                    Human xxx = humans[j];
                    humans[j] = humans[j + 1];
                    humans[j + 1] = xxx;
                }
            }
        }
        for (int i = 0; i < humans.length; i++) {
            System.out.println(humans[i]);
        }
    }
}
