package HashMap;

import java.util.HashMap;
import java.util.Scanner;

public class Main {

        // Карл у Клары украл кларнет. Карл плохой мальчик!

    public static void main(String[] args) {

        HashMap<String, Integer> map = new HashMap<>();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите строку");
        String str = scanner.nextLine();
        String[] strings = str.split(" ");

        for (String s : strings) {
            if (map.containsKey(s)) {
                map.put(s, map.get(s) + 1);
            } else {
                map.put(s, 1);
            }
        }
        System.out.println(map);
    }
}

