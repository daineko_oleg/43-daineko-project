package HashSet;

import java.util.HashSet;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        HashSet<Number> num = new HashSet<>();

        while (true) {
            System.out.println("Введите Ваш регион");
            String region = scanner.nextLine();

            System.out.println("Введите номер, который Вы хотите");
            String value = scanner.nextLine();

            Number number = new Number(value, region);

            if (num.contains(number)) {
                System.out.println("Такой номер существует, введите другой");
            } else {
                num.add(number);
                System.out.println("Ваш номер зарегистрирован");
            }
        }
    }
}
