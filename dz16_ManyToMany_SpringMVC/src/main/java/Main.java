import model.Student;
import model.Teacher;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import service.WorkingWithDB;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        WorkingWithDB workingWithDB = new WorkingWithDB();

        //workingWithDB.createStudentTeacher(); // +
        //workingWithDB.viewingAllStudent();  // +
        //workingWithDB.viewingAllTeacher();  // +
        //workingWithDB.viewingStudentHisTeachers(1L);  // +
        //workingWithDB.viewingTeacherHisStudents(2L);  // +

        workingWithDB.printInfo();
        workingWithDB.select();
    }
}
