package service;


import model.Student;
import model.Teacher;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.Scanner;
import java.util.*;
import java.util.function.Consumer;

public class WorkingWithDB {

    private Configuration configuration = new Configuration();
    private Scanner scanner = new Scanner(System.in);

    public WorkingWithDB() {
        configuration.configure();
    }

    // создаем студентов, преподавателей, таблицы и кто у кого преподает и учится
    public void createStudentTeacher() {

        try (SessionFactory sessionFactory = configuration.buildSessionFactory();
             Session session = sessionFactory.openSession()) {

            session.getTransaction().begin();

            Student student1 = Student.builder()
                    .firstName("er")
                    .lastName("re")
                    .patronymic("ee")
                    .build();
            Student student2 = Student.builder()
                    .firstName("rt")
                    .lastName("tr")
                    .patronymic("tt")
                    .build();
            Student student3 = Student.builder()
                    .firstName("ty")
                    .lastName("yt")
                    .patronymic("yy")
                    .build();
            Student student4 = Student.builder()
                    .firstName("yu")
                    .lastName("uy")
                    .patronymic("uu")
                    .build();
            Student student5 = Student.builder()
                    .firstName("ui")
                    .lastName("iu")
                    .patronymic("ii")
                    .build();

            Teacher teacher1 = Teacher.builder()
                    .firstName("qa")
                    .lastName("aq")
                    .patronymic("qq")
                    .build();
            Teacher teacher2 = Teacher.builder()
                    .firstName("as")
                    .lastName("sa")
                    .patronymic("aa")
                    .build();
            Teacher teacher3 = Teacher.builder()
                    .firstName("sz")
                    .lastName("zs")
                    .patronymic("ss")
                    .build();
            Teacher teacher4 = Teacher.builder()
                    .firstName("zx")
                    .lastName("xz")
                    .patronymic("zz")
                    .build();

            session.save(student1);
            session.save(student2);
            session.save(student3);
            session.save(student4);
            session.save(student5);

            session.save(teacher1);
            session.save(teacher2);
            session.save(teacher3);
            session.save(teacher4);

            teacher1.setStudents(new ArrayList<>(Arrays.asList(student1, student2)));
            teacher2.setStudents(new ArrayList<>(Arrays.asList(student3, student4)));
            teacher3.setStudents(new ArrayList<>(Arrays.asList(student2, student4)));

            student1.setTeachers(new ArrayList<>(Arrays.asList(teacher1, teacher2)));
            student2.setTeachers(new ArrayList<>(Arrays.asList(teacher2, teacher3)));
            student3.setTeachers(new ArrayList<>(Arrays.asList(teacher1, teacher3)));
            student4.setTeachers(new ArrayList<>(Arrays.asList(teacher2)));

            session.getTransaction().commit();
        }
    }

    // просматриваем всех студентов
    public void viewingAllStudent() {

        try (SessionFactory sessionFactory = configuration.buildSessionFactory();
             Session session = sessionFactory.openSession()) {

            session.getTransaction().begin();

            Query<Student> studentQuery = session.createQuery("from Student", Student.class);
            List<Student> students = studentQuery.list();

            for (Student student : students) {
                System.out.println(student);
            }
            session.getTransaction().commit();
        }
    }

    public void viewingAllTeacher() {

        try (SessionFactory sessionFactory = configuration.buildSessionFactory();
             Session session = sessionFactory.openSession()) {

            session.getTransaction().begin();

            Query<Teacher> teacherQuery = session.createQuery("from Teacher", Teacher.class);
            List<Teacher> teacherList = teacherQuery.list();

            for (Teacher teacher : teacherList) {
                System.out.println(teacher);
            }
            session.getTransaction().commit();
        }
    }

    // студент и его преродаватели
    public void viewingStudentHisTeachers(Long studentID) {

        try (SessionFactory sessionFactory = configuration.buildSessionFactory();
             Session session = sessionFactory.openSession()) {

            session.getTransaction().begin();

            Student student = session.get(Student.class, studentID);

            System.out.println("Студент: " + student.getFirstName() + " " + student.getLastName() +
                    " " + student.getPatronymic());
            System.out.println("Преподаватели: ");
            student.getTeachers().forEach(
                    teacher -> System.out.println(teacher.getFirstName() + " " + teacher.getLastName() +
                            " " + teacher.getPatronymic()));
            session.getTransaction().commit();
        }
    }

    // преподаватель и его студенты
    public void viewingTeacherHisStudents(Long teacherID) {

        try (SessionFactory sessionFactory = configuration.buildSessionFactory();
             Session session = sessionFactory.openSession()) {

            session.getTransaction().begin();

            Teacher teacher = session.get(Teacher.class, teacherID);

            System.out.println("Преподаватель: " + teacher.getFirstName() + " " + teacher.getLastName() +
                    " " + teacher.getPatronymic());
            System.out.println("Студенты: ");
            teacher.getStudents().forEach(
                    student -> System.out.println(student.getFirstName() + " " + student.getLastName() +
                            " " + student.getPatronymic()));
            session.getTransaction().commit();
        }
    }

    public void printInfo() {
        System.out.println("Что бы просмотреть всех преподавателей, нажмите '1' \n" +
                "Что бы просмотреть всех студентов, нажмите '2' \n" +
                "Что бы просмотреть преподавателя с его студентами, нажмите '3' \n" +
                "Что бы просмотреть студента с его преподавателями, нажмите '4'");
    }

    public void select() {

        switch (scanner.nextInt()) {
            case 1:
                viewingAllTeacher();
                break;
            case 2:
                viewingAllStudent();
                break;
            case 3:
                System.out.println("Введите ID: ");
                long teacherID = scanner.nextInt();
                viewingTeacherHisStudents(teacherID);
                break;
            case 4:
                System.out.println("Введите ID: ");
                long studentID = scanner.nextInt();
                viewingStudentHisTeachers(studentID);
                break;
            default:
                System.out.println("Вы ввели неправильный номер");
        }
    }
}
