package model;

import lombok.*;
import model.Student;

import javax.persistence.*;
import java.util.List;


//@Data
@Getter
@Setter
@Entity
@Builder
@ToString
@Table(name="teacher")
@NoArgsConstructor
@AllArgsConstructor

public class Teacher {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name="first_name")
    private String firstName;
    @Column(name="last_name")
    private String lastName;
    @Column(name="patronymic")
    private String patronymic;

    @ToString.Exclude
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name="t_s", joinColumns = @JoinColumn(name="teacher_id"),
            inverseJoinColumns = @JoinColumn(name="student_id"))
    private List<Student> students;

}
