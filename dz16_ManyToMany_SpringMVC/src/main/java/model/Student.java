package model;

import lombok.*;

import javax.persistence.*;
import java.util.List;

//@Data
@Getter
@Setter
@ToString
@Entity
@Builder
@Table(name="student")
@NoArgsConstructor
@AllArgsConstructor

public class Student {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "patronymic")
    private String patronymic;

    @ToString.Exclude
    @ManyToMany(mappedBy="students")
    private List<Teacher> teachers;

}
