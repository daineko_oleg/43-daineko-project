//Реализовать два метода нахождения числа Фибоначчи.

//Оба метода принимают на вход число, которое обозначает порядковый номер числа Фибоначчи.
//Результат работы обоих методов - это число Фибоначчи, которое находится под данным номером.

// Пример:
//Ввели число 3 - метод вернет число 2
//Ввели числ 5 - метод вернет число 5
//Ввели число 10 - метод вернет число 55

import java.util.Scanner;

public class Solution {

    public static int fibonacci(int number1) {

        if (number1 <= 1) {
            return number1;
        } else {
            return fibonacci(number1 - 1) + fibonacci(number1 - 2);
        }
    }

    public static void main(String[] args) {

        int count = 0;

        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();

        int[] arr = new int[number];
        arr[0] = 0;
        arr[1] = 1;

        for (int i = 2; i < number; i++) {
            arr[i] = arr[i - 1] + arr[i - 2];
            count = arr[i] + arr[i - 1];
        }

        System.out.println(count);
        System.out.println(fibonacci(number));
    }
}
