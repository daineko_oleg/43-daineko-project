package mvc.controller;


import mvc.dto.CarDto;
import mvc.service.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@org.springframework.stereotype.Controller
@RequiredArgsConstructor
// будут отрабатываться запросы на регистрацию
public class SignUpController implements Controller {

    private final CarService carService;

   //@Autowired
    //public SignUpController(CarService carService) {
    //    this.carService = carService;
    //}

    @Override
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();

        if (request.getMethod().equalsIgnoreCase("post")) {
            CarDto carDto = CarDto.builder()
                    .brand(request.getParameter("brand"))
                    .model(request.getParameter("model"))
                    .color(request.getParameter("color"))
                    .build();
            carService.createCar(carDto);
            modelAndView.setViewName("cars");
        } else {
            modelAndView.setViewName("signUp");
        }
        return modelAndView;
    }
}
