package mvc.service;

import mvc.dto.CarDto;
import mvc.model.Car;

import java.util.List;

public interface CarService {

    void createCar(CarDto carDto);

    List<Car> getAllCars();
}
