package mvc.repository;

import mvc.model.Car;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CarRepositoryImpl implements CarRepository {

    private DataBaseHiber dataBaseHiber;

    public CarRepositoryImpl(DataBaseHiber dataBaseHiber) {
        this.dataBaseHiber = dataBaseHiber;
    }

    @Override
    public void save(Car car) {
        try (Session session = dataBaseHiber.getSession()) {
            session.save(car);
        } catch (RuntimeException re) {
            throw new RuntimeException(re);
        }
    }

    @Override
    public List<Car> getAllCars() {
        try (Session session = dataBaseHiber.getSession()) {
            NativeQuery<Car> query = session.createNativeQuery("select * from car", Car.class);
            return query.getResultList();
        } catch (RuntimeException re) {
            throw new RuntimeException(re);
        }
    }
}
