package mvc.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.Optional;

@Service
public class HiberSessionUtil implements DataBaseHiber {

    private final SessionFactory sessionFactory;
    //private Map<Session, Boolean> mapSession;

    @Autowired
    public HiberSessionUtil(SessionFactory sessionFactory) {
        this.sessionFactory = new Configuration().configure().buildSessionFactory();
    }

    //@PostConstruct
    //private void init() {
    //    mapSession.put(sessionFactory.openSession(), Boolean.TRUE);
    //    mapSession.put(sessionFactory.openSession(), Boolean.TRUE);
    //    mapSession.put(sessionFactory.openSession(), Boolean.TRUE);
    //    mapSession.put(sessionFactory.openSession(), Boolean.TRUE);
    //}

    @Override
    public Session getSession() {

    //    Optional<Session> session = mapSession.keySet().stream()
    //            .filter(s -> mapSession.get(s) == Boolean.TRUE)
    //            .findFirst();

    //    if (session.isPresent()) {
    //        mapSession.put(session.get(), Boolean.FALSE);
    //        return session.get();
    //    }
        return sessionFactory.openSession();
    }
}
