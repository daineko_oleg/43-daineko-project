package mvc.repository;

import mvc.model.Car;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarRepository {

    void save(Car car);

    List<Car>getAllCars();
}
