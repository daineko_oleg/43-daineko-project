import auto_project.FileStorage;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

        FileStorage fileStorage = new FileStorage();

        fileStorage.printInfo();
        fileStorage.select();
    }
}
