package auto_project;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface Storage {

void warehouseCars() throws IOException;                                       // наличие авто
Car getByID(int id) throws IOException;                                        // поиск авто
Car[] addCar(Car newCar) throws IOException;                                    // добавить авто
void update();                                                                  // редактировать авто
void deleteCar();                                                                // удалить авто

}
