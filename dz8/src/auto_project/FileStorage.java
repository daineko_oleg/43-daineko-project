package auto_project;


import java.io.*;
import java.util.Scanner;

public class FileStorage implements Storage {

    private Car car= new Car();
    private Car[] cars = new Car[20];
    private File file = new File("resources/file.txt");
    private BufferedReader bR;
    private BufferedWriter bW;
    private FileWriter fW;
    private Scanner scanner = new Scanner(System.in);

    @Override
    public void warehouseCars() {

        cars[0] = new Car(car.getId(), "Москвич", "412", "желтый", 15);
        cars[1] = new Car(car.getId(), "Ваз", "2102", "синий", 20);
        cars[2] = new Car(car.getId(), "БМВ", "Х1", "красный", 11);
        cars[3] = new Car(car.getId(), "Мерседес", "А1", "белый", 25);
        cars[4] = new Car(car.getId(), "Тойота", "Камри", "черный", 21);
    //    cars[5] = new Car(car.getId(), "КИА", "Рио", "черный", 18);
    //    cars[6] = new Car(car.getId(), "Фольксваген", "Поло", "синий", 17);
    //    cars[7] = new Car(car.getId(), "Ситроен", "С4", "белый", 19);
    //    cars[8] = new Car(car.getId(), "Ауди", "А4", "черный", 13);
    //    cars[9] = new Car(car.getId(), "Шкода", "Октавиа", "желтый", 22);
    }

     public void warehouseCarsInFile() throws IOException {

        fW = new FileWriter("resources/file.txt");
        bW = new BufferedWriter(fW);

        for(int i = 0; i < cars.length; i++) {
            if(cars[i] != null) {
                bW.write(cars[i].toString() + "\n");
                bW.flush();
            }
        }
    }

    public void printwarehouseCarsInFile() throws IOException {

        bR = new BufferedReader(new FileReader("resources/file.txt"));

        String str = bR.readLine();
        while (str != null) {
            System.out.println(str);
            str = bR.readLine();
        }
    }

    @Override
    public Car getByID(int id) {
        for(Car c: cars) {
            if(c != null && c.getId() == id) {
                return c;
            }
        }
        return null;
    }

    public int countCars(Car[]cars) {
        int count = 0;
        for (Car c: cars) {
            if (c != null)
                count ++;
        }
        return count;
    }

    @Override
    public Car[] addCar(Car newCar) {
        cars[countCars(cars) + 1] = newCar;
        return cars;
    }

    @Override
    public void update() {

        System.out.println("Введите id");
        int carID = scanner.nextInt();
        for(Car c: cars) {
            if (c != null && c.getId() == carID) {
                scanner.nextLine();
                System.out.println("Измените бренд авто");
                c.setBrand(scanner.nextLine());
                System.out.println("Измените модель авто");
                c.setModel(scanner.nextLine());
                System.out.println("Измените цвет авто");
                c.setColor(scanner.nextLine());
                System.out.println("Измените мощность двигателя");
                c.setEnginePower(scanner.nextInt());

                System.out.println("Новые данные об авто");
                System.out.println(c);
            }
        }
    }

    @Override
    public void deleteCar() {

        int count = 0;
        System.out.println("Введите id");
        int carID = scanner.nextInt();
        for(Car c: cars) {
            count++;
            if (c != null && c.getId() == carID) {

                System.out.println("Удаляется автомобиль: " + c);

                cars[count - 1] = c;
                cars[count - 1] = null;
            }
        }
    }

    public void printInfo() {
        System.out.println(
            "Что бы получить информацию о наличии авто, нажмите '1' \n" +
            "Что бы найти автомобиль по ID, нажмите '2' \n" +
            "Что бы добавить новый автомобиль, нажмите '3' \n" +
            "Что бы редактировать данные об автомобиле, нажмите '4' \n" +
            "Чтобы удалить данные об автомобиле, нажмите '5' \n" );
    }

    public void select() throws IOException {
        switch (scanner.nextInt()) {
            case 1:
                warehouseCars();
                warehouseCarsInFile();
                printwarehouseCarsInFile();
                break;
            case 2:
                warehouseCars();
                System.out.println("Введите ID: ");
                Car car = getByID(scanner.nextInt());
                System.out.println(car);
                break;
            case 3:
                Car newCar = new Car();
                warehouseCars();
                warehouseCarsInFile();
                addCar(newCar);

                newCar.setId(countCars(cars) - 1);

                scanner.nextLine();
                System.out.println("Введите бренд авто");
                newCar.setBrand(scanner.nextLine());

                System.out.println("Введите модель авто");
                newCar.setModel(scanner.nextLine());

                System.out.println("Введите цвет авто");
                newCar.setColor(scanner.nextLine());

                System.out.println("Введите мощность двигателя");
                newCar.setEnginePower(scanner.nextInt());

                bW.write(newCar + "\n");
                bW.flush();

                //cars[countCars(cars) + 1] = newCar;

                System.out.println(newCar);
                break;
            case 4:
                warehouseCars();
                warehouseCarsInFile();
                update();
                warehouseCarsInFile();
                break;
            case 5:
                warehouseCars();
                warehouseCarsInFile();
                deleteCar();
                warehouseCarsInFile();
                break;
            default:
                System.out.println("Вы ввели неправильный номер");
                break;
        }
    }
}
