package auto_project;

import java.io.*;

public class Car {
    private static int COUNT_ID = 0;
    private int id;
    private String brand;
    private String model;
    private String color;
    private int enginePower;

    public Car() {
    }
    public Car(int id, String brand, String model, String color, int enginePower) {
        this.id = COUNT_ID;
        this.brand = brand;
        this.model = model;
        this.color = color;
        this.enginePower = enginePower;
        COUNT_ID++;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    //public String getBrand() {
    //    return brand;
    //}
    public void setBrand(String brand) {
        this.brand = brand;
    }

    //public String getModel() {
    //    return model;
    //}
    public void setModel(String model) {
        this.model = model;
    }

    //public String getColor() {
    //    return color;
    //}
    public void setColor(String color) {
        this.color = color;
    }

    //public int getEnginePower() {
    //    return enginePower;
    //}
    public void setEnginePower(int enginePower) {
        this.enginePower = enginePower;
    }

    public String toString() {
        return " ID: " + id + "," + " Бренд: " + brand + "," + " Модель: " + model + "," +
                " Цвет " + color + "," + " Мощность двигателя: " + enginePower + " л.с.";
    }
}

