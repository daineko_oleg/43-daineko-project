package repository;

import dto.SignUpForm;
import models.Account;

import java.util.ArrayList;
import java.util.List;

public class AccountsRepositoryImpl implements AccountsRepository {

    public final static List<Account> accounts = new ArrayList<>();
    private static Long countId = 0L;

    public void save(SignUpForm form) {
        Account account = Account.builder()
                .id(countId++)
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .email(form.getEmail())
                .password(form.getPassword())
                .build();

        accounts.add(account);
    }
    @Override
    public boolean checkEmailPassword(String email, String password) {
        return accounts.stream()
                .anyMatch(account -> account.getEmail().equals(email) && account.getPassword().equals(password));
    }

}
