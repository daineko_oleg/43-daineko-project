package dto;

import lombok.*;

@Data
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SignInForm {

    private String email;
    private String password;

}
