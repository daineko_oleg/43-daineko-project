package services;

import dto.SignInForm;

public interface SignInService {

    boolean checkEmailPassword(String email,String password);

}
