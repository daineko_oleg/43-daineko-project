package services;

import dto.SignInForm;
import repository.AccountsRepository;

public class SignInServiceImpl implements SignInService {

   private AccountsRepository accountsRepository;

    public SignInServiceImpl(AccountsRepository accountsRepository) {
        this.accountsRepository = accountsRepository;
    }

    @Override
    public boolean checkEmailPassword(String email, String password) {
        return accountsRepository.checkEmailPassword(email,password);
    }

}
