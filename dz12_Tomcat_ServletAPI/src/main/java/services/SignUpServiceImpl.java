package services;

import dto.SignUpForm;
import repository.AccountsRepository;

public class SignUpServiceImpl implements SignUpService {

    private AccountsRepository accountsRepository;

    public SignUpServiceImpl(AccountsRepository accountsRepository) {
        this.accountsRepository = accountsRepository;
    }

    public void save(SignUpForm form) {
        accountsRepository.save(form);
    }
}
