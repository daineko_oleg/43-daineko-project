<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>SignIn</title>
</head>
<body>
<h1>Sign In Page</h1>
<h2>Please Enter Your Data:</h2>
<form action="/signIn" method="post">
    <label for="email">Enter your email</label>
    <input id="email" type="email" name="email" placeholder="Your email">
    <br>
    <label for="password">Enter your password</label>
    <input id="password" type="password" name="password" placeholder="Your password">
    <input type="submit" value="Sign In!">
</form>
</body>
</html>