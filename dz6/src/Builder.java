

public class Builder extends Worker {

    public Builder(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }

    @Override
    public void goToWork() {
        System.out.println(name + " " + lastName + " " + "профессия " + profession + " - строю кого небудь");
    }

    @Override
    public void goToVacation(int days) {
        System.out.println(name + " " + lastName + " " + "профессия " + profession + ": отпуск " + days + " дней");
    }
}
