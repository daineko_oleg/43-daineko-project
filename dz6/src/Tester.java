

public class Tester extends Worker {

    public Tester(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }

    @Override
    public void goToWork() {
        System.out.println(name + " " + lastName + " " + "профессия " + profession + " - тестирую чего небудь");
    }

    @Override
    public void goToVacation(int days) {
        System.out.println(name + " " + lastName + " " + "профессия " + profession + ": отпуск " + days + " дней");
    }
}
