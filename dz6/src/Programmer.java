

public class Programmer extends Worker {

    public Programmer(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }

    @Override
    public void goToWork() {
        System.out.println(name + " " + lastName + " " + "профессия " + profession + " - типа пишу код");
    }

    @Override
    public void goToVacation(int days) {
        System.out.println(name + " " + lastName + " " + "профессия " + profession + ": отпуск " + days + " дней");
    }
}
