

public class Driver extends Worker {

    public Driver(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }
    @Override
    public void goToWork() {
        System.out.println(name + " " + lastName + " " + "профессия " + profession + " - водитель кобылы");
    }
    @Override
    public void goToVacation(int days) {
        System.out.println(name + " " + lastName + " " +"профессия " + profession + ": отпуск " + days + " дней");
    }

}
