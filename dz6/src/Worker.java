

public class Worker {

    String name;
    String lastName;
    String profession;

    public Worker(String name, String lastName, String profession) {
        this.name = name;
        this.lastName = lastName;
        this.profession = profession;
    }


    public void goToWork() {
        System.out.println(name + lastName + "профессия: " + profession);
        System.out.println(profession + "- ...");
    }

    public void goToVacation(int days) {
        System.out.println(name + lastName + "профессия: " + profession + ": отпуск " + days + "дней");
        System.out.println("...");
    }

}
