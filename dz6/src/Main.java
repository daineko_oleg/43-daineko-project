public class Main {

    public static void main(String[] args) {

        Worker driver = new Driver("Степан", "Иванов", "водитель");
        Worker builder = new Builder("Иван", "Петров", "строитель");
        Worker tester = new Tester("Николай", "Степанов", "тестировщик");
        Worker programmer = new Programmer("Василий", "Сидоров", "программист");

        driver.goToWork();
        driver.goToVacation(10);

        builder.goToWork();
        builder.goToVacation(15);

        tester.goToWork();
        tester.goToVacation(30);

        programmer.goToWork();
        programmer.goToVacation(60);

    }
}
