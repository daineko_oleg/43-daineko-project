package tele;

import java.util.Random;
import java.util.Scanner;

public class PultM implements Pult {

    private Tv tv;

    public PultM(Tv tv) {
        this.tv = tv;
    }

    public Tv getTv() {
        return tv;
    }

    public void powerTV() {
        tv.powerTV();
    }

    @Override
    public void pereklPoSchifram() {
        tv.pereklPoSchifram();
    }

    @Override
    public void pereklVpered() {
        tv.pereklVpered();
    }

    @Override
    public void pereklNazad() {
        tv.pereklNazad();
    }

    @Override
    public void pereklMezdyPoslednimiKanalami() {
        tv.pereklMezdyPoslednimiKanalami();
    }
}
