package tele;

// Класс Канал должен иметь поля:
//* Название канала
//* Массив передач

import java.util.Random;

public class Channel {

    private String nameChannel;
    private ProgramTV[] program;

    public Channel(String nameChannel, ProgramTV[] program) {
        this.nameChannel = nameChannel;
        this.program = program;
    }

    public ProgramTV[] getProgramTV() {
        return program;
    }

    @Override
    public String toString() {
        return nameChannel;
    }

    public String getRandomNameProgram() {
        Random random = new Random();
        ProgramTV[] programs = getProgramTV();
        int rdNum = random.nextInt(programs.length);
        return programs[rdNum].getNameProgram();
    }
}
