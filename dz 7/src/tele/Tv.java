package tele;


import java.util.Random;
import java.util.Scanner;

//Класс Телевизор должен иметь поля:
//* Массив каналов
public class Tv {

    private Channel[] channels;
    private Random random = new Random();
    private Scanner scanner = new Scanner(System.in);

    public Tv(Channel[] channels) {
        this.channels = channels;
    }

    //  вкл телевизора
    public void powerTV() {

        int num1 = random.nextInt(channels.length);
        String nameProgram = channels[num1].getRandomNameProgram();
        System.out.println("Вы смотрите канал: " + channels[num1]);
        System.out.println("Идет передача: " + nameProgram);
        System.out.println(channels.length);
    }

    public void pereklPoSchifram() {

        System.out.println("-------------------------------------");
        System.out.println("Выберите канал");

        int num2 = scanner.nextInt();

        if (num2 >= channels.length) {
            num2 = 1;
        }

        String nameProgram = channels[num2 - 1].getRandomNameProgram();

        System.out.println("Вы смотрите канал: " + channels[num2 - 1]);
        System.out.println("Идет передача: " + nameProgram);
    }

    public void pereklVpered() {

        int num3 = random.nextInt(channels.length);

        String nameProgram = channels[num3 - 1].getRandomNameProgram();

        System.out.println("-------------------------------------");
        System.out.println("Вы смотрите канал: " + channels[num3 - 1]);
        System.out.println("Идет передача: " + nameProgram);

        System.out.println("");
        System.out.println("Что бы переключиться на следующий канал, нажмите ' 0 '");

        int num4 = scanner.nextInt();

        String nameProgram1 = channels[num3].getRandomNameProgram();

        if (num4 == 0) {
            if (num3 > channels.length) {
                num3 = 0;
            }
            System.out.println("Вы смотрите канал: " + channels[num3]);
            System.out.println("Идет передача: " + nameProgram1);
        }
    }

    public void pereklNazad() {

        int num5 = random.nextInt(channels.length);
        String nameProgram = channels[num5 - 1].getRandomNameProgram();

        System.out.println("-------------------------------------");
        System.out.println("Вы смотрите канал: " + channels[num5 - 1]);
        System.out.println("Идет передача: " + nameProgram);

        System.out.println("");
        System.out.println("Что бы переключиться на предыдущий канал, нажмите ' - 1 '");

        int num6 = scanner.nextInt();
        String nameProgram1 = channels[num5].getRandomNameProgram();

        if (num6 == -1) {
            if (num5 - 2 < 0) {
                num5 = channels.length + 1;
            }
            System.out.println("Вы смотрите канал: " + channels[num5 - 2]);
            System.out.println("Идет передача: " + nameProgram1);
        }
    }

    public void pereklMezdyPoslednimiKanalami() {

        int num7 = random.nextInt(channels.length);
        String nameProgram = channels[num7 - 1].getRandomNameProgram();

        System.out.println("-------------------------------------");
        System.out.println("Вы смотрите канал: " + channels[num7 - 1]);
        System.out.println("Идет передача: " + nameProgram);

        System.out.println("");
        System.out.println("Выберите канал");

        int num8 = scanner.nextInt();

        if (num8 >= channels.length) {
            num8 = 1;
        }
        String nameProgram1 = channels[num8 - 1].getRandomNameProgram();

        System.out.println("Вы смотрите канал: " + channels[num8 - 1]);
        System.out.println("Идет передача: " + nameProgram1);

        System.out.println("------------");
        System.out.println("Что бы переключится на предыдущий канал, нажмите ' - 2 '");

        int num9 = scanner.nextInt();

        if (num9 == -2) {
            System.out.println("Вы смотрите канал: " + channels[num7 - 1]);
            System.out.println("Идет передача: " + nameProgram);
        }

        System.out.println("------------");
        System.out.println("Что бы переключится на предыдущий канал, нажмите '- 2 '");

        int num10 = scanner.nextInt();

        if (num10 == -2) {
            System.out.println("Вы смотрите канал: " + channels[num8 - 1]);
            System.out.println("Идет передача: " + nameProgram1);
        }
    }
}

