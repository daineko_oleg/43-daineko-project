package tele;


import java.util.Random;
import java.util.Scanner;

// Класс Телевизор должен иметь поля:
//* Массив каналов
//* Пульт
public class Televizor implements Pult {

    String[] myCanal = {"РТР", "Первый", "НТВ", "ТНТ", "Фигвам", "Не наш", "Ага два раза",
            "Куку в печке", "Гусь подхребетный", "Клюква жареная"};

    public void power() {
        Random random = new Random();
        Canal canal = new Canal();
        Peredacha peredacha = new Peredacha();

        int num1 = random.nextInt(myCanal.length);
        int num2 = random.nextInt(canal.getMyPeredacha().length);

        canal.nameCanal = myCanal[num1];
        peredacha.namePeredacha = canal.getMyPeredacha()[num2];
                                                   // myCanal[num1]
        System.out.println("Вы смотрите канал: " + canal.nameCanal);
                                                   // canal.getMyPeredacha()[num2]
        System.out.println("Идет передача: " + peredacha.namePeredacha);
    }

    @Override
    public void pereklPoSchifram() {

        Random random = new Random();
        Canal canal = new Canal();
        Peredacha peredacha = new Peredacha();

        System.out.println("-------------------------------------");
        System.out.println("Выберите канал");

        Scanner scanner = new Scanner (System.in);
        int num = scanner.nextInt();

        canal.nameCanal = myCanal[num];
        peredacha.namePeredacha = canal.getMyPeredacha()[random.nextInt(canal.getMyPeredacha().length)];

        System.out.println("Вы смотрите канал: " + canal.nameCanal);
        System.out.println("Идет передача: " + peredacha.namePeredacha);
    }

    @Override
    public void pereklVpered() {

        Random random = new Random();
        Canal canal = new Canal();
        Peredacha peredacha = new Peredacha();

        Scanner scanner = new Scanner (System.in);
        int num = random.nextInt(myCanal.length);

        canal.nameCanal = myCanal[num];
        peredacha.namePeredacha = canal.getMyPeredacha()[random.nextInt(canal.getMyPeredacha().length)];

        System.out.println("-------------------------------------");
        System.out.println("Вы смотрите канал: " + canal.nameCanal);
        System.out.println("Идет передача: " + peredacha.namePeredacha);

        System.out.println("");
        System.out.println("Что бы переключить канал вперед, нажмите ' 0 '");

        int num3 = scanner.nextInt();

        if(num3 == 0) {
            System.out.println("Вы смотрите канал: " + myCanal[num + 1]);
            System.out.println("Идет передача: " + canal.getMyPeredacha()
                    [random.nextInt(canal.getMyPeredacha().length)]);
        }
    }

    @Override
    public void pereklNazad() {

        Random random = new Random();
        Canal canal = new Canal();
        Peredacha peredacha = new Peredacha();

        Scanner scanner = new Scanner (System.in);
        int num = random.nextInt(myCanal.length);

        canal.nameCanal = myCanal[num];
        peredacha.namePeredacha = canal.getMyPeredacha()[random.nextInt(canal.getMyPeredacha().length)];

        System.out.println("-------------------------------------");
        System.out.println("Вы смотрите канал: " + canal.nameCanal);
        System.out.println("Идет передача: " + peredacha.namePeredacha);

        System.out.println("");
        System.out.println("Что бы переключить канал назад, нажмите ' - 1 '");

        int num4 = scanner.nextInt();

        if(num4 == - 1) {
            canal.nameCanal = myCanal[num - 1];
            peredacha.namePeredacha = canal.getMyPeredacha()
                    [random.nextInt(canal.getMyPeredacha().length)];
            System.out.println("Вы смотрите канал: " + canal.nameCanal);
            System.out.println("Идет передача: " + peredacha.namePeredacha);
        }
    }

    @Override
    public void pereklMezdyPoslednimiKanalami() {

        Random random = new Random();
        Canal canal = new Canal();
        Peredacha peredacha = new Peredacha();

        Scanner scanner = new Scanner (System.in);
        int num = random.nextInt(myCanal.length);

        canal.nameCanal = myCanal[num];
        peredacha.namePeredacha = canal.getMyPeredacha()[random.nextInt(canal.getMyPeredacha().length)];

        System.out.println("-------------------------------------");
        System.out.println("Вы смотрите канал: " + canal.nameCanal);
        System.out.println("Идет передача: " + peredacha.namePeredacha);

        System.out.println("");
        System.out.println("Переключите канал");

        int num5 = scanner.nextInt();

        canal.nameCanal1 = myCanal[num5];
        peredacha.namePeredacha1 = canal.getMyPeredacha()[random.nextInt(canal.getMyPeredacha().length)];

        System.out.println("Вы смотрите канал: " + canal.nameCanal1);
        System.out.println("Идет передача: " + peredacha.namePeredacha1);

        System.out.println("------------");
        System.out.println("Что бы переключится на предыдущий канал, нажмите ' -2 '");

        int num6 = scanner.nextInt();

        if(num6 == - 2) {
            System.out.println("Вы смотрите канал: " + canal.nameCanal);
            System.out.println("Идет передача: " + peredacha.namePeredacha);
        }

        System.out.println("------------");
        System.out.println("Что бы переключится на предыдущий канал, нажмите '-2'");

        int num7 = scanner.nextInt();

        if (num7 == -2) {
            System.out.println("Вы смотрите канал: " + canal.nameCanal1);
            System.out.println("Идет передача: " + peredacha.namePeredacha1);
        }
    }
    public static void main(String[] args) {

        Pult pult = new Televizor();

        ((Televizor) pult).power();
        pult.pereklPoSchifram();
        pult.pereklVpered();
        pult.pereklNazad();
        pult.pereklMezdyPoslednimiKanalami();
    }
}

