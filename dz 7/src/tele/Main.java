package tele;

public class Main {

    public static void main(String[] args) throws NullPointerException {

        ProgramTV[] programChannel1 = {new ProgramTV("Проблема пустого холодильника"),
                new ProgramTV("Суп из топора"), new ProgramTV("Репа пареная")};
        ProgramTV[] programChannel2 = {new ProgramTV("Работа не волк"),
                new ProgramTV("Зарплата нахаляву"), new ProgramTV("Лень всему голова")};
        ProgramTV[] programChannel3 = {new ProgramTV("Типа новости"),
                new ProgramTV("Катаклизм на кухне"), new ProgramTV("Сказки на ночь")};
        ProgramTV[] programChannel4 = {new ProgramTV("Посланные дважды"),
                new ProgramTV("Недождетесь"), new ProgramTV("Мысли навылет")};
        ProgramTV[] programChannel5 = {new ProgramTV("Вынос мозга задаром"),
                new ProgramTV("Эрудит поневоле"), new ProgramTV("Козел, птица певчая")};

        Channel channel1 = new Channel("Куку в печке", programChannel1);
        Channel channel2 = new Channel("Гусь подхребетный", programChannel2);
        Channel channel3 = new Channel("Клюква жареная", programChannel3);
        Channel channel4 = new Channel("Ага два раза", programChannel4);
        Channel channel5 = new Channel("Фигвам, а не нам", programChannel5);

        Channel[] channels = {channel1, channel2, channel3, channel4, channel5};

        Tv tv = new Tv(channels);
        PultM pult = new PultM(tv);

        pult.powerTV();

        pult.pereklPoSchifram();
        pult.pereklVpered();
        pult.pereklNazad();
        pult.pereklMezdyPoslednimiKanalami();
    }
}
