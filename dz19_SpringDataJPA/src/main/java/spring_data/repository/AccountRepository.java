package spring_data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import spring_data.model.Account;

import java.time.LocalDate;
import java.util.List;

public interface AccountRepository extends JpaRepository<Account, Long> {

    List<Account> findBySubscriptionsIsNull();
    List<Account> findByBalanceGreaterThan(Long balance);
    @Query("SELECT aсс FROM Account aсс WHERE SIZE(aсс.subscriptions) = :count")
    List<Account> findBySubscriptionCount(@Param("count") int count);
    List<Account> findBySubscriptionsName(String subscriptionName);
    List<Account> findBySubscriptionsCostGreaterThan(Long cost);
    @Query("SELECT acc FROM Account acc JOIN acc.subscriptions sub WHERE sub.before > :date")
    List<Account> findBySubscriptionsBeforeAfter(@Param("date") LocalDate date);

}
