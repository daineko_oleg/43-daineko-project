package spring_data.repository;

import spring_data.model.Account;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class AccountRepositoryEntityManagerImpl implements AccountRepositoryOldSolution {

    //Основной интерфейс ORM, который служит для управления персистентными сущностями
    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    @Override
    public void save(Account account) {
        entityManager.persist(account);
    }

    @Override
    public List<Account> getAllAccounts() {
        return null;
    }
}
