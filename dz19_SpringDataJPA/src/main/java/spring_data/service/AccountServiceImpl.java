package spring_data.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring_data.dto.AccountDto;
import spring_data.model.Account;
import spring_data.repository.AccountRepository;

import java.time.LocalDate;
import java.util.List;


@Service
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;

    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public void saveAccount(AccountDto accountDto) {
        Account account = Account.builder()
                .firstName(accountDto.getFirstName())
                .lastName(accountDto.getLastName())
                .patronymic(accountDto.getPatronymic())
                .email(accountDto.getEmail())
                .balance(accountDto.getBalance())
                .build();

        accountRepository.save(account);
    }

    @Override
    public List<Account> getAllAccounts() {
        return accountRepository.findAll();
    }

    @Override
    public List<Account> findBySubscriptionsIsNull() {
        return accountRepository.findBySubscriptionsIsNull();
    }

    @Override
    public List<Account> findByBalanceGreaterThan(Long balance) {
        return accountRepository.findByBalanceGreaterThan(balance);
    }

    @Override
    public List<Account> findBySubscriptionCount(int count) {
        return accountRepository.findBySubscriptionCount(count);
    }

    @Override
    public List<Account> findBySubscriptionsName(String subscriptionName) {
        return accountRepository.findBySubscriptionsName(subscriptionName);
    }

    @Override
    public List<Account> findBySubscriptionCostGreaterThan(Long cost) {
        return accountRepository.findBySubscriptionsCostGreaterThan(cost);
    }

    @Override
    public List<Account> findBySubscriptionsStillValidDate(LocalDate endDate) {
        return accountRepository.findBySubscriptionsBeforeAfter(endDate);
    }
}
