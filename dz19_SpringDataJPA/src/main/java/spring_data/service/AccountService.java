package spring_data.service;

import spring_data.dto.AccountDto;
import spring_data.model.Account;

import java.time.LocalDate;
import java.util.List;

public interface AccountService {

    void saveAccount(AccountDto accountDto);

    List<Account> getAllAccounts();
    List<Account> findBySubscriptionsIsNull();
    List<Account> findByBalanceGreaterThan(Long balance);
    List<Account> findBySubscriptionCount(int count);
    List<Account> findBySubscriptionsName(String subscriptionName);
    List<Account> findBySubscriptionCostGreaterThan(Long cost);
    List<Account> findBySubscriptionsStillValidDate(LocalDate endDate);

}
