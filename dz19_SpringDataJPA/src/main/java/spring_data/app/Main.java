package spring_data.app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import spring_data.config.ApplicationConfig;
import spring_data.dto.AccountDto;
import spring_data.service.AccountService;

import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {

        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        AccountService accountService = (AccountService) applicationContext.getBean("accountServiceImpl");

        //AccountDto accountDto = AccountDto.builder()
        //        .firstName("jj")
        //        .lastName("jjj")
        //        .patronymic("j")
        //        .email("jj@jj.com")
        //        .balance(Long.valueOf("250"))
        //        .build();

        //accountService.saveAccount(accountDto);

        //все аккаунты
        //System.out.println(accountService.getAllAccounts());

        //аккаунты без подписок
        //System.out.println(accountService.findBySubscriptionsIsNull());

        //аккаунты с балансом больше 240 уе
        //System.out.println(accountService.findByBalanceGreaterThan(240L));

        //аккаунты с 1-ой подпиской
        //System.out.println(accountService.findBySubscriptionCount(1));

        //аккаунты с подпиской "yt"
        //System.out.println(accountService.findBySubscriptionsName("yt"));

        //аккаунты с подпиской стоимостью больше 80 уе
        //System.out.println(accountService.findBySubscriptionCostGreaterThan(80L));

        //аккаунты действующие на 2024-01-01
        System.out.println(accountService.findBySubscriptionsStillValidDate(LocalDate.ofEpochDay(2024-01-01)));

    }
}
