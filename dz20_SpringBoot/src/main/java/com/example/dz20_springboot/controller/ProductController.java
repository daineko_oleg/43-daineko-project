package com.example.dz20_springboot.controller;


import com.example.dz20_springboot.dto.ProductDto;
import com.example.dz20_springboot.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.ui.Model;

import javax.validation.Valid;

@Controller
@RequestMapping(value = "/signUp") //указали на какой url будет реагировать данный контроллер
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService  = productService;
    }

    //@GetMapping
    @RequestMapping(method = RequestMethod.GET)
    public String getSignUpPage(Model model) {
        model.addAttribute("productDto", new ProductDto());
        return "signUp";
    }

    //@PostMapping
    @RequestMapping(method = RequestMethod.POST)
    public String signUp(@Valid ProductDto productDto, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("productDto", productDto);
            return "signUp";
        }
        productService.saveProduct(productDto);
        return "redirect:/signUp";
    }
}
