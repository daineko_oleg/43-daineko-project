package com.example.dz20_springboot.service;

import com.example.dz20_springboot.dto.ProductDto;

public interface ProductService {

    void saveProduct(ProductDto productDto);

}
