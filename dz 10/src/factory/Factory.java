package factory;

import java.util.concurrent.Semaphore;

public class Factory {

    private FaceControl faceControl;

    public Factory(FaceControl faceControl) {
        this.faceControl = faceControl;
    }

    public void toWork() throws InterruptedException {
        faceControl.got_aPass();
    }
    public void fromWork(){
        faceControl.returnedThePass();
    }
}
