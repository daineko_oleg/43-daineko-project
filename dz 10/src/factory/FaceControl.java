package factory;

import java.util.concurrent.Semaphore;

public class FaceControl {

    private Semaphore semaphore;

    public FaceControl(Semaphore semaphore) {
        this.semaphore = semaphore;
    }

    public void got_aPass() throws InterruptedException {            // получил пропуск
        semaphore.acquire();
    }
    public void returnedThePass(){                                  //вернул пропуск
        semaphore.release();
    }

}