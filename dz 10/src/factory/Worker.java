package factory;


public class Worker extends Thread {

    private String nameWorker;

    private Factory factory;

    public Worker(String nameWorker, Factory factory) {
        this.nameWorker = nameWorker;
        this.factory = factory;
    }

    @Override
    public void run() {
        System.out.println(nameWorker + " пришел на завод");

        try {

            System.out.println(nameWorker + " ждет пропуск");
            factory.toWork();
            System.out.println(nameWorker + " получил пропуск");
            Thread.sleep(200);
            System.out.println(nameWorker + " работает");
            factory.fromWork();
            System.out.println(nameWorker + " отдал пропуск");
            Thread.sleep(200);
            System.out.println(nameWorker + " ушел домой");

        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}

