package factory;

import java.util.concurrent.Semaphore;

public class Main {

    public static void main(String[] args) {

        Semaphore semaphore = new Semaphore(3);

        FaceControl faceControl = new FaceControl(semaphore);
        Factory factory = new Factory(faceControl);


        new Worker("nameWorker1", factory).start();
        new Worker("nameWorker2", factory).start();
        new Worker("nameWorker3", factory).start();
    }
}
